DateTimeZoneUtils =
{

	/**
	 * This is a method to get the current Date/Time based on the time zone
	 * offset specified.
	 * 
	 * @param timeZoneOffSet
	 *            {Number} - The time zone offset.
	 * 
	 * @returns {Date} date - The Date/Time Object based on the time zone offset
	 *          specified.
	 */
	getCurrentDateTime : function(timeZoneOffSet)
	{
		if ((timeZoneOffSet == null && timeZoneOffSet == '' && timeZoneOffSet == undefined) || isNaN(timeZoneOffSet))
			return new Date();

		var currentDateTime = new Date();
		var UTC = currentDateTime.getTime() + (currentDateTime.getTimezoneOffset() * 60000);
		currentDateTime = UTC + (timeZoneOffSet * 60 * 60 * 1000);

		return new Date(currentDateTime);
	},

	/**
	 * This is a method to get the current Date/Time based on a passed time zone
	 * offset converted to the specified Date/Time string format.
	 * 
	 * @param timeZoneOffSet
	 *            {Number} - The time zone offset.
	 * @param nsDateFormat
	 *            {String} - The Date/Time format string to convert the date to.
	 * 
	 * @returns {String} dateString - The Date/Time Object based on the time
	 *          zone offset specified converted to the Date/Time string format
	 *          specified.
	 */
	getCurrentDateTimeText : function(timeZoneOffSet, nsDateFormat)
	{
		return nlapiDateToString(this.getCurrentDateTime(timeZoneOffSet), nsDateFormat);
	},

	/**
	 * This is a method to get the current Date/Time based on the time zone
	 * selected on the Company Preferences page.
	 * 
	 * @returns {Date} date - The Date/Time Object based on the time zone
	 *          selected under the Company Preferences page.
	 */
	getCompanyCurrentDateTime : function()
	{
		var currentDateTime = new Date();
		var companyTimeZone = nlapiLoadConfiguration('companyinformation').getFieldText('timezone');
		var timeZoneOffSet = (companyTimeZone.indexOf('(GMT)') == 0) ? 0 : new Number(companyTimeZone.substr(4, 6).replace(/\+|:00/gi, '').replace(/:30/gi, '.5'));
		var UTC = currentDateTime.getTime() + (currentDateTime.getTimezoneOffset() * 60000);
		var companyDateTime = UTC + (timeZoneOffSet * 60 * 60 * 1000);

		return new Date(companyDateTime);
	},

	/**
	 * This is a method to get the current Date/Time based on the time zone
	 * selected on the Company Preferences page converted to a specified
	 * Date/Time string format.
	 * 
	 * @param nsDateFormat
	 *            {String} - The Date/Time format string to convert the date to.
	 * 
	 * @returns {String} dateString - The Date/Time Object based on the time
	 *          zone selected under the Company Preferences page converted to
	 *          the Date/Time string format specified.
	 */
	getCompanyCurrentDateTimeText : function(nsDateFormat)
	{
		return nlapiDateToString(this.getCompanyCurrentDateTime(), nsDateFormat);
	},

	/**
	 * This is a method to get the current Date/Time based on the time zone
	 * selected on the Subsidiary passed as a parameter.
	 * 
	 * @param subsidiary
	 *            {Number} - The subsidiary internal id.
	 * 
	 * @returns {Date} date - The Date/Time Object based on the time zone
	 *          selected under the Subsidiary record specified.
	 */
	getSubsidiaryCurrentDateTime : function(subsidiary)
	{
		var currentDateTime = new Date();
		var subsidiaryTimeZone = nlapiLoadRecord('subsidiary', subsidiary).getFieldText('TIMEZONE');
		var timeZoneOffSet = (subsidiaryTimeZone.indexOf('(GMT)') == 0) ? 0 : new Number(subsidiaryTimeZone.substr(4, 6).replace(/\+|:00/gi, '').replace(/:30/gi, '.5'));
		var UTC = currentDateTime.getTime() + (currentDateTime.getTimezoneOffset() * 60000);
		var subsidiaryDateTime = UTC + (timeZoneOffSet * 60 * 60 * 1000);

		return new Date(subsidiaryDateTime);
	},

	/**
	 * This is a method to get the current Date/Time based on the time zone
	 * selected on the Subsidiary passed as a parameter converted to the
	 * Date/Time String format specified.
	 * 
	 * @param subsidiary
	 *            {Number} - The subsidiary internal id.
	 * @param nsDateFormat
	 *            {String} - The Date/Time format string to convert the date to.
	 * 
	 * @returns {String} dateString - The Date/Time Object based on the time
	 *          zone selected under the Subsidiary record converted to the
	 *          Date/Time string format specified.
	 */
	getSubsidiaryCurrentDateTimeText : function(timeZoneOffSet, nsDateFormat)
	{
		return nlapiDateToString(this.getCurrentDateTime(timeZoneOffSet), nsDateFormat);
	},
};