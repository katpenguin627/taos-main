var EXTHEADERS = [];
EXTHEADERS['NETOAPI_ACTION'] = 'AddItem';
EXTHEADERS['NETOAPI_USERNAME'] = 'nsconnector';
EXTHEADERS['NETOAPI_KEY'] = 'c8HA0MVHnVZnnmeviqOypU1REfVe5nhs';
EXTHEADERS['Accept'] = 'application/json';

var NETO_ENDPOINT_URL = 'http://teknikmotorsport.com.au/do/WS/NetoAPI';

var NETO_PRIMARY_WAREHOUSE = '1';
var NS_WAREHOUSE_LOC_ID = '2';

/**
 *
 */
function afterSubmit_SyncProductToNeto(type) {

    try {

        dLog('afterSubmit_SyncProductToNeto', 'type = ' + type);
        dLog('afterSubmit_SyncProductToNeto', 'nlapiGetRecordType() = ' + nlapiGetRecordType());
        dLog('afterSubmit_SyncProductToNeto', 'nlapiGetRecordId() = ' + nlapiGetRecordId());

        if (type == 'create' || type == 'edit') {

            var recId = nlapiGetRecordId();
            var rec = nlapiLoadRecord(nlapiGetRecordType(), recId);

            if (type == 'create') {

                createNetoProduct(rec, recId);

            }
            else {

                updateNetoProduct(rec, recId);
            }

            nlapiSubmitField(nlapiGetRecordType(), recId, 'custitem_sync_to_neto', 'F');
        }
    }
    catch (e) {
        var stErrMsg = '';
        if (e.getDetails != undefined) {
            stErrMsg = 'Script Error: ' + e.getCode() + '<br>' + e.getDetails() + '<br>' + e.getStackTrace();
        }
        else {
            stErrMsg = 'Script Error: ' + e.toString();
        }

        dLog('Script Error', stErrMsg);
    }
}

/**
 *
 * @param rec
 */
function createNetoProduct(rec, id) {

    var jsonReq = {
        Item : []
    };

    var objJSON = {};
    eval('objJSON = ' + JSON.stringify(rec));
    dLog('createNetoProduct', 'objJSON = ' + JSON.stringify(objJSON));

    var parentItem = rec.getFieldValue('parent');
    var itemRec = nlapiLookupField('item', id, ['pricesincludetax', 'quantityavailable']);
    var netoItemName = '';
    var qtyAvl = '';

    if (nlapiGetRecordType() == 'inventoryitem') {
        netoItemName = rec.getFieldValue('displayname');
        qtyAvl = itemRec.quantityavailable;
    }
    else {
        netoItemName = rec.getFieldValue('displayname');
        qtyAvl = getKitQtyAvailable(id);
    }
    
    var sku = rec.getFieldValue('itemid');
	sku = (sku.indexOf(':') != -1) ? sku.split(':')[1].trim() : sku;
	var tax;
	if(itemRec.pricesincludetax=='T'){
		tax = true;
	}
	else {
		tax =false;
	}
    var objItem = {
        SKU : sku,
        Active : (rec.getFieldValue('isonline') == 'T') ? true : false,
        //Misc03 : id,
        Name : netoItemName,
        ShippingWeight : rec.getFieldValue('weight'),
        // as per Tammy [10:02:14 AM] Tammy Warburton: Yes so for the Parent, we
        // need to change this in netSuite to grab the info in
        // custitem_os_parent_sku
        // ParentSKU : (!isEmpty(parentItem)) ? nlapiLookupField('item',
        // parentItem, 'itemid') : '',
        ParentSKU : rec.getFieldValue('custitem_os_parent_sku'),
        RRP : nlapiLookupField('item', id, 'baseprice'),
        //Description : rec.getFieldValue('storedescription'),
        Brand : rec.getFieldValue('custitemos_brand'),
        TaxInclusive : tax,
        AutomaticURL : true,
        Categories : addCategories(rec, false),
        PriceGroups : addPriceGroups(rec),
        WarehouseQuantity : [{
            WarehouseID : NETO_PRIMARY_WAREHOUSE,
            Quantity : qtyAvl,
            Action : 'set'
        }],
        Subtitle :  rec.getFieldValue('storedescription'),
        Description : rec.getFieldValue('storedetaileddescription'),
        SEOPageTitle : rec.getFieldValue('pagetitle'),
        Misc01 : rec.getFieldValue('custitem5'),
        ShippingWeight : rec.getFieldValue('weight'),
        TaxInclusive: tax,
        Misc03 : rec.getFieldText('custitem16'),
        //DefaultPrice: rec.getFieldValue('cost'),
        CostPrice: rec.getFieldValue('cost'),
       
    };

    jsonReq.Item.push(objItem);

    dLog('createNetoProduct', 'jsonReq = ' + JSON.stringify(jsonReq));

    var connect = nlapiRequestURL(NETO_ENDPOINT_URL, JSON.stringify(jsonReq), EXTHEADERS, 'POST');
    var responseCode = connect.getCode();
    var responseBody = connect.getBody();
    var reponseEror = connect.getError();

    dLog('createNetoProduct', 'responseCode = ' + responseCode);
    dLog('createNetoProduct', 'responseBody = ' + responseBody);
    dLog('createNetoProduct', 'reponseEror = ' + reponseEror);
}

/**
 *
 * @param rec
 */
function updateNetoProduct(rec, id) {

    EXTHEADERS['NETOAPI_ACTION'] = 'UpdateItem';

    var jsonReq = {
        Item : []
    };

    var objJSON = {};
    eval('objJSON = ' + JSON.stringify(rec));
    dLog('updateNetoProduct', 'objJSON = ' + JSON.stringify(objJSON));

    var parentItem = rec.getFieldValue('parent');
    var itemRec = nlapiLookupField('item', id, ['pricesincludetax', 'quantityavailable']);
    var netoItemName = '';
    var qtyAvl = '';

    if (nlapiGetRecordType() == 'inventoryitem') {
        netoItemName = rec.getFieldValue('displayname');
        qtyAvl = itemRec.quantityavailable;
    }
    else {
        netoItemName = rec.getFieldValue('displayname');
        qtyAvl = getKitQtyAvailable(id);
    }
    var tax;
	if(itemRec.pricesincludetax=='T'){
		tax = true;
	}
	else {
		tax =false;
	}
    var objItem = {
        SKU : rec.getFieldValue('itemid'),
        Active : (rec.getFieldValue('isonline') == 'T') ? true : false,
        //Misc03 : id,
        Name : netoItemName,
        ShippingWeight : rec.getFieldValue('weight'),
        // as per Tammy [10:02:14 AM] Tammy Warburton: Yes so for the Parent, we
        // need to change this in netSuite to grab the info in
        // custitem_os_parent_sku
        // ParentSKU : (!isEmpty(parentItem)) ? nlapiLookupField('item',
        // parentItem, 'itemid') : '',
        ParentSKU : rec.getFieldValue('custitem_os_parent_sku'),
        RRP : nlapiLookupField('item', id, 'baseprice'),
        Description : rec.getFieldValue('storedescription'),
        Brand : rec.getFieldValue('custitemos_brand'),
        TaxInclusive : tax,
        Categories : addCategories(rec, true),
        PriceGroups : addPriceGroups(rec),
        WarehouseQuantity : [{
            WarehouseID : NETO_PRIMARY_WAREHOUSE,
            Quantity : qtyAvl,
            Action : 'set'
        }],
        Subtitle :  rec.getFieldValue('storedescription'),
        Description : rec.getFieldValue('storedetaileddescription'),
        SEOPageTitle : rec.getFieldValue('pagetitle'),
        Misc01 : rec.getFieldValue('custitem5'),
        ShippingWeight : rec.getFieldValue('weight'),
        TaxInclusive: tax,
        Misc03 : rec.getFieldText('custitem16'),
        //DefaultPrice: rec.getFieldValue('cost'),
        CostPrice: rec.getFieldValue('averagecost'),  
        
    };

    jsonReq.Item.push(objItem);

    dLog('updateNetoProduct', 'jsonReq = ' + JSON.stringify(jsonReq));

    var connect = nlapiRequestURL(NETO_ENDPOINT_URL, JSON.stringify(jsonReq), EXTHEADERS, 'POST');
    var responseCode = connect.getCode();
    var responseBody = connect.getBody();
    var reponseEror = connect.getError();

    dLog('updateNetoProduct', 'responseCode = ' + responseCode);
    dLog('updateNetoProduct', 'responseBody = ' + responseBody);
    dLog('updateNetoProduct', 'reponseEror = ' + reponseEror);
}

// Netsuite product sample id = 3676
function addKitComponent(id) {

    var KitComponents = {
        KitComponent : []
    };

    var rs = getKitComponents(id);

    for (var i = 0; rs != null && i < rs.length; i++) {

        objJson.KitComponents.KitComponent.push({
            ComponentSKU : rs[i].getText('memberitem'),
            // ComponentValue : '',
            AssembleQuantity : rs[i].getValue('memberquantity'),
            MinimumQuantity : rs[i].getValue('minimumquantity'),
            // MaximumQuantity : 0
        });
    }

    dLog('addKitComponent', 'KitComponents = ' + JSON.stringify(KitComponents));

    return KitComponents;
}

function getKitQtyAvailable(stItemId) {

    var filters = [new nlobjSearchFilter('inventorylocation', 'memberitem', 'anyOf', NS_WAREHOUSE_LOC_ID), new nlobjSearchFilter('internalid', null, 'anyOf', stItemId)];
    var columns = [new nlobjSearchColumn('formulanumeric', null, 'min').setFormula("nvl({memberitem.locationquantityavailable},0)/{memberquantity}")];
    var rs = nlapiSearchRecord('kititem', null, filters, columns);

    return (rs) ? rs[0].getValue('formulanumeric', null, 'min') : 0;
}

/**
 *
 */
function addPriceGroups(rec) {

    var PriceGroups = {
        PriceGroup : []
    };
    for (var i = 1; i <= rec.getLineItemCount('price1'); i++) {

        var grpName = rec.getLineItemValue('price1', 'pricelevelname', i);
        PriceGroups.PriceGroup.push({
            Group : (grpName == 'Base Price') ? 'Retail' : grpName,
            Price : rec.getLineItemValue('price1', 'price_1_', i),
          	MinimumQuantity : rec.getFieldValue('minimumquantity'),
            Multiple : rec.getFieldValue('minimumquantity')
        });
    }

    dLog('addPriceGroups', 'PriceGroups = ' + JSON.stringify(PriceGroups));

    return PriceGroups;
}
//TEST ME
function addCategories(rec, isUpdate) {

    var Categories = {
        Category : []
    };

    for (var i = 1; i <= rec.getLineItemCount('sitecategory'); i++) {

        if (isUpdate) {

            Categories.Category.push({
                CategoryID : rec.getLineItemValue('sitecategory', 'categorydescription', i),
                Priority : '0'
            });
        }
        else {

            Categories.Category.push({
                CategoryID : rec.getLineItemValue('sitecategory', 'categorydescription', i)
            });
        }
    }

    dLog('addCategories', 'Categories = ' + JSON.stringify(Categories));

    return Categories;
}

function getPrices(rec) {

    var arrPrices = [];
    // var arrPLNames = getPLNames();

    for (var i = 1; i <= rec.getLineItemCount('price'); i++) {
        arrPrices.push({
            plvl_id : rec.getLineItemValue('price', 'pricelevel', i),
            plvl_name : rec.getLineItemValue('price', 'pricelevelname', i),
            price : rec.getLineItemValue('price', 'price_1_', i)
        });
    }

    // dLog('getPrices', 'arrPrices = ' + JSON.stringify(arrPrices));

    return arrPrices;
}

function getPrice(rec) {

    var price = '';

    for (var i = 1; i <= rec.getLineItemCount('price'); i++) {

        var priceLvl = rec.getLineItemValue('price', 'pricelevel', i);

        // check if price level is ONLINE
        if (priceLvl == BASE_PRICE_LVL) {
            price = rec.getLineItemValue('price', 'price_1_', i);
            break;
        }

    }

    // dLog('getPrices', 'arrPrices = ' + JSON.stringify(arrPrices));

    return price;
}

function getPLNames() {
    var arrRet = [];
    var rs = nlapiSearchRecord('pricelevel', null, null, new nlobjSearchColumn('name'));

    for (var i = 0; rs != null && i < rs.length; i++) {
        arrRet[rs[i].getId()] = rs[i].getValue('name');
    }

    return arrRet;
}

function getKitComponents(kitId) {

    var filters = [new nlobjSearchFilter('internalid', null, 'anyOf', kitId)];
    var columns = [];
    columns.push(new nlobjSearchColumn('memberitem'));
    columns.push(new nlobjSearchColumn('memberquantity'));
    columns.push(new nlobjSearchColumn('minimumquantity'));

    var rs = nlapiSearchRecord('kititem', null, filters, columns);

    return rs;
}

// ***helper functions***

function setValue(fldValue) {
    if (isEmpty(fldValue))
        return '';

    return fldValue;
}

function isEmpty(fldValue) {
    return fldValue == '' || fldValue == null || fldValue == undefined;
}

function dLog(logTitle, logDetails) {
    nlapiLogExecution('DEBUG', logTitle, logDetails);
}